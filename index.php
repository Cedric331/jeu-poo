<?php 
function chargement($classe)
{
   require 'classe/' . $classe . '.php';
}

spl_autoload_register('chargement');


$perso1 = new Personnage("Joe", 50);
$perso2 = new Personnage("Judith", 40);

$info = $perso1 -> info();
echo $info;

$info = $perso2 -> info();
echo $info;

$attaque = $perso1 -> frapper($perso2);
echo $attaque;

$experience = $perso1 -> gagnerExperience();
echo $experience;




?>