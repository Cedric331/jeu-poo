<?php 

class Personnage
   {
   private $_nom;
   private $_force;
   private $_localisation = 'New York';
   private $_experience = 1;
   private $_sante = 0;


      public function __construct($nom, $force)
      {
         $this -> _force = $force;
         $this -> _nom = $nom;
      }

      public function info()
      {
         $info = "<br><h3>Information du Personnage : </h3>" .
         "Nom: " . $this -> _nom .
         "<br>Force: " . $this -> _force .
         "<br>Localisation: " . $this -> _localisation .
         "<br>Expérience: " . $this -> _experience .
         "<br>Santé: " . $this -> _sante . "<br>";
         return $info;
      }

      public function frapper(Personnage $cible)
      {
         $cible -> _sante += $this -> _force;
         return "<br><h4>Combat</h4><br>Le personnage inflige " . $this -> _force . " de dégâts à la cible <br>"; 
      }

      public function deplacer()
      {

      }

      public function gagnerExperience()
      {
         $this -> _experience++;
         return "<br>Le personnage gagne 1 point d'expérience";
      }
   }

?>